<?php

//**** Module Definition ****

// Name of the module. The module name must match the name of the module directory.
// The module name may not contain spaces.
$module['name']      = 'wizard';

// Title of the module which is dispalayed in the top navigation.
$module['title']     = 'Wizard';

// The template file of the module. This is always 'module.tpl.htm' unless
// there are any special requirements such as a three column layout.
$module['template']  = 'module.tpl.htm';

// The page that is displayed when the module is loaded.
// The path must is relative to the web/ directory
$module['startpage'] = 'wizard/new_service.php';

// The width of the tab. Normally you should leave this empty and
// let the browser define the width automatically.
$module['tab_width'] = '';

//****  Menu Definition ****

// Make sure that the items array is empty
$items = array();

// Add a menu item with the label 'Send message'
$items[] = array( 'title'   => 'New service wizard',
                  'target'  => 'content',
                  'link'    => 'wizard/new_service.php'
                );
                
$items[] = array( 'title'   => 'Templates',
                  'target'  => 'content',
                  'link'    => 'wizard/template_list.php'
                );
                            

// Append the menu $items defined above to a menu section labeled 'Support'
$module['nav'][] = array( 'title' => 'Wizard',
                          'open'  => 1,
                          'items'	=> $items
                        );