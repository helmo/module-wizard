
ALTER TABLE `wizard_template`
    ADD COLUMN `proxy_protocol` ENUM('n','y') NOT NULL DEFAULT 'n' AFTER `log_retention`;
ALTER TABLE `wizard_template`
    ADD COLUMN `server_php_id` int(11) UNSIGNED NOT NULL DEFAULT 0;


ALTER TABLE `wizard_template` ADD `backup_interval` varchar(255) NOT NULL DEFAULT 'none' AFTER `custom_php_ini`;
ALTER TABLE `wizard_template` ADD `backup_copies` int(11) NOT NULL DEFAULT 1 AFTER `backup_interval`;

-- backup format
ALTER TABLE `wizard_template` ADD  `backup_format_web` VARCHAR( 255 ) NOT NULL default 'default' AFTER `backup_copies`;
ALTER TABLE `wizard_template` ADD  `backup_format_db` VARCHAR( 255 ) NOT NULL default 'gzip' AFTER `backup_format_web`;
-- end of backup format

-- backup encryption
ALTER TABLE `wizard_template` ADD  `backup_encrypt` enum('n','y') NOT NULL DEFAULT 'n' AFTER `backup_format_db`;
ALTER TABLE `wizard_template` ADD  `backup_password` VARCHAR( 255 ) NOT NULL DEFAULT '' AFTER `backup_encrypt`;
-- end of backup encryption

ALTER TABLE `wizard_template` ADD `backup_excludes` mediumtext DEFAULT NULL AFTER `backup_password`;

ALTER TABLE `wizard_template` ADD  `jailkit_chroot_app_sections` mediumtext NULL DEFAULT NULL;
ALTER TABLE `wizard_template` ADD  `jailkit_chroot_app_programs` mediumtext NULL DEFAULT NULL;
ALTER TABLE `wizard_template` ADD  `delete_unused_jailkit` enum('n','y') NOT NULL DEFAULT 'n';
ALTER TABLE `wizard_template` ADD  `last_jailkit_update` date NULL DEFAULT NULL;
ALTER TABLE `wizard_template` ADD  `last_jailkit_hash` varchar(255) DEFAULT NULL;

-- Match incremental/upd_0091.sql
ALTER TABLE `wizard_template` ALTER pm SET DEFAULT 'ondemand';
ALTER TABLE `wizard_template` DROP COLUMN `enable_spdy`;
ALTER TABLE `wizard_template` ADD `folder_directive_snippets` TEXT NULL AFTER `https_port`;
ALTER TABLE `wizard_template` CHANGE `apache_directives` `apache_directives` mediumtext NULL DEFAULT NULL;
ALTER TABLE `wizard_template` CHANGE `nginx_directives` `nginx_directives` mediumtext NULL DEFAULT NULL;

-- Match incremental/upd_0092.sql
ALTER TABLE `wizard_template` DROP COLUMN `fastcgi_php_version`;

-- Match incremental/upd_0100.sql
ALTER TABLE `wizard_template` ADD `disable_symlinknotowner` enum('n','y') NOT NULL default 'n' AFTER `last_jailkit_hash`;
UPDATE `wizard_template` SET `backup_format_web` = 'tar_gzip' WHERE  `backup_format_web` = 'rar';
UPDATE `wizard_template` SET `backup_format_db` = 'zip' WHERE  `backup_format_db` = 'rar';

-- Extra field
ALTER TABLE `wizard_template` ADD `template_notes` TEXT NOT NULL default '' AFTER `template_name`;

-- Include ssl checkboxes
ALTER TABLE `wizard_template` ADD `ssl` enum('n','y') NOT NULL DEFAULT 'n' AFTER `rewrite_to_https`;
ALTER TABLE `wizard_template` ADD `ssl_letsencrypt` enum('n','y') NOT NULL DEFAULT 'n' AFTER `ssl`;

-- Restore old column, setting up a new slave server now gets old datalog to be processed which fails on this col missing.
ALTER TABLE `wizard_template` ADD `fastcgi_php_version` varchar(255) DEFAULT NULL COMMENT "deprecated";
