# ISPConfig Wizard module

This module allows you to create DNS Zone, Site, Database User, Database, FTP User, SSH User, Mail Domain and Mailboxes in one form. You just define domain name and using checkboxes decide which services you want to create. Services are created using user defined templates.

This module is compatible with ISPConfig version 3.2

## Installation

This module has to be installed on master node in multiserver setup.

- Download this repo as archive, unpack it to `/usr/local/ispconfig/interface/web/` and rename forlder to `wizard` OR  
clone repo using git `git clone https://git.ispconfig.org/ispconfig/module-wizard.git /usr/local/ispconfig/interface/web/wizard`
- Create the database table provided in `db.sql` file: `mysql -u root -p < db.sql`
- Enable module in user interface System -> CP Users -> Admin user -> Check "wizard" and save.
  - If it doesn't work, enable module manually by editing admin user in DB table `sys_user` column `modules`

On a slave server you only have to create the database table using the instructions above.

## Upgrade

When upgrading from an older version:

- Replace existing files of the module with new ones (this doesn't apply to slave servers in multiserver setup)
- Make sure that the database schema is up to date. Apply `db_update_3.2.sql`, or, should that fail, execute relevant statements from it manually
- Please note that in the database, `fastcgi_php_version` string column has been replaced with `server_php_id`, which is an integer. This means you will have to modify all your saved wizard configuration templates after the update to make sure that they use the correct PHP version

## Planned features

- Export results as PDF
- Email results as HTML/PDF

## Known bugs

- DKIM generation doesn't respect mail server dkim_strength configuration. Instead is used default value of 2048

## Contributing

Contributions are welcome, so please contribute your translations. Thank you.

[Contributing guide](https://git.ispconfig.org/ispconfig/module-wizard/blob/master/CONTRIBUTING.md)

## Screenshots

![New service](https://git.ispconfig.org/ispconfig/module-wizard/raw/master/readme_images/new_service.png)

![Template](https://git.ispconfig.org/ispconfig/module-wizard/raw/master/readme_images/template.png)

![Result](https://git.ispconfig.org/helmo/module-wizard/raw/master/readme_images/result.png)
